#!/usr/bin/perl
#
# $Id: header.pl 28 2005-07-26 18:12:41Z erickson $
# $Source: /home/erickson/java.ici/public_html/NetDist/bin/RCS/header.pl,v $
# put name of program and short description here
#
=pod

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 OPTIONS

=cut
BEGIN {
    require "$ENV{'NDHOME'}/etc/NetConf.pl";
    require "$ENV{'NDHOME'}/etc/site.pl";
}
use Netg;
use Site::Hosts;
use Site::Networks;
$ng = new Netg "$NDHOME/etc/netgroup";
$sh = new Site::Hosts;
$nw = new Site::Networks;
#
$TMPF = ".tmp.$$";
push(@NDTMPFILES,$TMPF);
#
#
# parse the args
use Getopt::Std;
&getopts("V");
$NDVERBOSE = $opt_V if ( defined($opt_V));
#
# Get a lock on the file so no one else uses it
&GetLock($NDMYNAME);
&CleanUp($NDMYNAME);
exit(0);
