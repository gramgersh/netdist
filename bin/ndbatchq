#!/usr/bin/perl
#
# $Id: ndbatchq 28 2005-07-26 18:12:41Z erickson $
# $Source: /home/erickson/java.ici/public_html/NetDist/bin/RCS/ndbatchq,v $
#
=pod

=head1 NAME

ndbatchq -- process the batch queue

=head1 SYNOPSIS

ndbatch [C<-V>]

=head1 DESCRIPTION

Process the batch queue, and update the $NDHOSTINST file.

The $NDHOSTINST file is read in.  Then, for each file in the batch
queue, the corresponding lines are deleted from the $HOSTINST file,
and the new versions of the lines are added.

The lines deleted are those with the same ``key'' as the lines in the
batch file.  See B<hostinst> (4) for the format of the hostinst file.

=head1 OPTIONS

=over 4

=item C<-V>

Print out status information as the queue is processed.

=back

=cut

BEGIN {
    require  "$ENV{'NDHOME'}/etc/NetConf.pl";
}
#
# check directories
&CheckDirs($NDBATCHDIR);
chdir($NDBATCHDIR) || &errorexit("$NDMYNAME:  cannot cd to $NDBATCHDIR: $!");
#
# Parse the args and see if we're in verbose mode
use Getopt::Std;
&getopts("V");
Verb::ose($opt_V);
#
# See if another ndbatchque is running.  Do a timeout if so
&GetLock();
#
# We'll be reading complete files, so disable the record separator.
undef $/;	# read the entire file
#
# Get the host instruction file.  Lock the file so that nothing
# changes the file after we start.
&GetLock("hostinst");
if ( -s $NDHOSTINST ) {
    &errorexit("Cannot open $NDHOSTINST: $!\n")
	if (!open(INFILE,"$NDHOSTINST"));
    $HostInstData = <INFILE> ; 
    close INFILE;
}
#
# At this point, we've got our Host Instruction file in the $HostInstData var.
#
# Put an initial \n in the HostInstData to make substitutions easier.
$HostInstData = "\n" . $HostInstData ;
#
# Make sure that there are two \n's between each record.  We'll use this
# to remove old records.
$HostInstData =~ s/\n/\n\n/g;
# Study the $HostInstData, as we'll be doing lots of pattern matching on it.
study $HostInstData;
#
# Process each batch file.
while ( &GetFirstBatch()) {
    local($ThisKey);
    #
    # Get a lock file so no one else will work on this batch file
    #
    # A policy decision is being made here:  if a batch file is
    #   busy, then assume that someone's writing it.  Skip the rest of
    #   the batch files.
    Verb::EchoC("$NDMYNAME:  processing batch $FILE . . . ",1);
    if ( &GetTmpLock("batch.$FILE") != 0) {
	&ErrorEcho("batch file $FILE busy,");
	&ErrorEcho("$NDMYNAME:    skipping rest of batch files");
	last ;
    }
    #
    # Ok.  Now we can read in the batch file.
    &errorexit("Cannot open $FILE: $!\n")
	if (!open(INFILE,"$FILE"));
    $BatchData = <INFILE>;
    close INFILE;
    $BatchData =~ s/\n/\n\n/g;
    #
    # Get the key from the file, and remove all related files
    # from the current HostInstruction file.  This is why we put the
    # initial \n at the beginning of the $HostInstData.
    #
    # The data we're looking at is like:  \nRemotehost Key Other-Stuff\n
    # The perl manual says that '.' matches anything but newline.
    #
    $ThisKey = (split(/\s+/,$BatchData))[1];
    $HostInstData =~ s\n[^\s]*\s+$ThisKey\s+.*\ng ;
    #
    # At this point, our $HostInstData has *no* data for the current
    # subsystem.  Now, just append the data from the batch file.
    $HostInstData .= $BatchData;
    #
    # remove the batchfile and the lock on the batch file
    unlink("$FILE");
    &RmTmpLock();
    Verb::Echo("done",1);
}
#
# We're ready to write out the new HostInstFile.  Make sure to backup
# the old one.
Verb::EchoC("$NDMYNAME:  making copy of hostinst file . . . ",1);
rename($NDHOSTINST, $NDHOSTINST . '.bak') if ( -e $NDHOSTINST );
Verb::Echo("done",1);
#
# Now, create the new file
Verb::EchoC("$NDMYNAME:  adding new instructions to hostinst file . . .",1);
&errorexit("Cannot open $NDHOSTINST for write: $!\n")
    if ( !open(OUTFILE,">$NDHOSTINST") );
# get rid of the initial \n
$HostInstData =~ s/^\n+//;
# replace multiple \n's with 1
$HostInstData =~ s/\n+/\n/g;
# get rid of any end-of-line space
$HostInstData =~ s/\s+\n/\n/g;
print OUTFILE $HostInstData;
close(OUTFILE);
Verb::Echo("done",1);
#
# clean up
&CleanUp($NDMYNAME);
#######################################################################
#######################################################################
# GetFirstBatch()
#    returns the name of the first batch file in the queue.
# We want to run this every time rather than once in order to account
# for batch files that may have been added while we were processing
# earlier batch files.
#
sub GetFirstBatch {
    local($fname,@FILES);

    @FILES = <[0-9]*>;
    @FILES = sort {$a <=> $b;} @FILES ;
    $FILE=shift(@FILES);
    return ("$FILE" ne "") ;
}
