#
# $Id: Networks.pm 28 2005-07-26 18:12:41Z erickson $
# $Source: /opt/NetDist/elp/lib/perl/Site/RCS/Networks.pm,v $
#
package Site::Networks;
do "$ENV{'NDHOME'}/etc/NetConf.pl";
do "$NDHOME/etc/site.pl";

=head1 NAME

=head1 SYNOPSIS

	use Site::NetWorks;
	$nw = new Site::Networks("name") ;
	$nw->set("name");	# set current object to name
	$nw->name();	        # return name of current network
	$nw->ip();	        # return IP address of network
	$nw->netmask();		# return netmask
	$nw->netmaskbits() ;	# return number of bits in netmask
	$nw->hostmask() ;	# return hostmask
	$nw->hostmaskbits() ;	# return number of bits in host mask
	$nw->broadcast();	# return broadcast address
	$nw->setnetent() ;	# rewind to beginning of networks
	$nw->getnetent() ;	# return next network number
	$nw->networks() ;	# return a list of all networks
	$nw->ipclass();	        # return IP Class of network ('A' - 'C')
	$nw->valid_hosts() ;	# return a list of all valid IP numbers
	$nw->hfc_host($ip) ;	# return a name for an HFC host on this net
	$nw->hfc_textid() ;	# return a string as a text ID
	$nw->tas_name() ;	# return the name of a tas
	$nw->router_name() ;	# return the name of a router
	$nw->tas_ip() ;		# return the ip of a tas
	$nw->router_ip() ;	# return the ip of a router
	$nw->block2nets("a.b.c.d/X:Y") ;# return array of /Y nets from block /X
	$nw->sortip(@iplist) ;  # return array sorted by ip addr
	$nw->networkpart($ip,$mask) ;  # return the network part of the ipaddr
	$nw->hostpart($ip,$mask) ;  # return the host part of the ipaddr



=head1 DESCRIPTION

The C<Site::Networks> method provides access to the host information for

    the NetDist site, which may be different than the information for

    the running site.

=cut

#
require 5.000;
use Carp;
#
#
# Network names.  We have %NetworksByName from site.pl.
@NetworkNames = ();
#
sub new {
    my $this = shift;
    my $class = ref($this) || $this;
    my $self = {};
    my $netname = shift;

    bless $self,$class;
    $self->set("$netname") if ( $netname ne "" ) ;
    $self;
}

sub set {
    my($self,$name) = @_;
    my($netnum,$bits,$newname,$splitstr);
    # See if a network number was sent
    if ( $name =~ /^\d+\.\d+\.\d+\.\d+/ ) {
	# If $newname is null, then we can only set the network
	# number.
	if ( ! ($newname = nameof($name) ) ) {
	    $splitstr = $name;
	}
	else {
	    $splitstr = $NetworksByName{$newname};
	    $name = $newname;
	}
    }
    else {
	# set our split string
	$splitstr = $NetworksByName{$name};
    }
    $self->{NAME} = $name;
    ($netnum,$bits) = split(/\//,$splitstr);
    $self->{IP} = $netnum;
    $self->{BITS} = $bits;
}

#
# sub name() -- return the name
sub name {my($self) = @_; return $self->{NAME};}
# sub ip() -- return the ip address
sub ip {my($self) = @_; return $self->{IP};}
# sub netmaskbits() -- return the number of bits in netmask
sub netmaskbits {my($self) = @_; return $self->{BITS};}
# sub hostmaskbits() -- return the number of bits in hostmask
sub hostmaskbits {my($self) = @_; return 32-$self->{BITS};}
# sub networks() -- return a list of networks
sub networks {return keys(%NetworksByName)};
# sub ipclass() -- return the ip class of address
sub ipclass {
    my($self,$addr) = @_;
    my($a) = (split(/\./,$addr))[0];
    return undef if ( $a eq "" );
    return 'A' if ( $a < 128 );
    return 'B' if ( $a < 192 );
    return 'C' if ( $a < 224 );
    return undef;
}
#
# sub valid_hosts() -- return an array of valid host numbers
sub valid_hosts {
    my($self) = @_;
    my($first,$lst);
    local(@hosts,$_);

    $first  = ip2l($self->ip());
    $lst = $first + (2**$self->hostmaskbits() - 1);
    $first++;	# skip the '0' address
    $lst--;     # skip the '255' address
    foreach ( $first..$lst ) {
	push(@hosts,l2ip(pack("L",$_)));
    }
    return @hosts;
}
#
# sub netmask() -- return the netmask
sub netmask {
    my($self) = @_;
    my($bits,@bytes,$i);

    $bits = $self -> {BITS};
    ($bytes[3],$bytes[2],$bytes[1],$bytes[0]) =
	unpack('C4',pack("b32", "0" x (32 - $bits) . "1" x $bits));
    return join(".",@bytes);
}
#
# sub hostmask() -- return the hostmask
sub hostmask {
    my($self) = @_;
    my($bits,@bytes,$i);

    $bits = 32 - $self -> {BITS};
    ($bytes[3],$bytes[2],$bytes[1],$bytes[0]) =
	unpack('C4',pack("b32", "1" x $bits . "0" x (32 - $bits)));
    return join(".",@bytes);
}
#
# sub broadcast() -- return the broadcast address
sub broadcast {
    my($self) = @_;
    return l2ip(pack("L",(ip2l($self->ip())+(2**$self->hostmaskbits())-1)));
}
#
# setnetent() set up the NetworkNames array
sub setnetent {
    my($self) = shift(@_);
    @NetworkNames = keys(%NetworksByName);
    return;
}
#
# getnetent() -- return the next network entry
sub getnetent {
    my($self) = shift(@_);
    local($name);
    return undef if ( $#NetworkNames < 0 );
    $name = shift(@NetworkNames);
    $self->set($name);
    return $name;
}
#
# nameof($addr) -- return the network name that goes with the address.
sub nameof {
    local($_);
    my($net,$mask,$ip);

    $ip = (split(/\//,shift(@_)))[0];
    foreach ( keys(%NetworksByName) ) {
	($net,$mask) = split(/\//, $NetworksByName{$_});
	return $_ if ( $net eq $ip );
    }
    return undef;
}
#
# sub ip2l($ip) -- change a dotted notation number into a long
sub ip2l {
    my($ip) = @_;
    return(unpack("L",pack("C4",split(/\./,$ip) )));
}
#
# sub l2ip($l) -- change the long into a dotted notation
sub l2ip {
    my($l) = @_;
    return(join(".",unpack("C4",$l)));
}
#
# sub hfc_host($ip) -- return an HFC name for host $ip on this
# network.  $ip can be either one byte or 4 bytes.
sub hfc_host {
    my($self,$ip) = @_;


    return sprintf("d%02x%02x%02x%02x", split(/\./,$ip));
}

#
# sub hfc_textid() -- return a string that can be used as a textual id
sub hfc_textid {
    my($self) = @_;
    my($net,$host,$city,$mcr) = split(/-/,$self->name());
    my($netnum) = $self->ip() . "/" . $self->netmaskbits ;
    if ( $net eq "hfc" ) {
	return uc($host) . "$city $mcr  $netnum";
    }
    if ( $net eq "nat" ) {
	return "NAT Network $host $netnum";
    }
    return uc($host) . " " . uc($net) . " Network on interface $iface";
}
    
#
# sub tas_name() -- return a string that can be used as a tas name
sub tas_name {
    my($self) = @_;
    my($jnk,$tas,$iface) = split(/-/,$self->name());
    $iface =~ s/:/-/g;
    return "$tas-$iface";
}
    
#
# sub router_name() -- return a string that can be used as a router name
sub router_name {
    my($self) = @_;
    my($jnk,$host,$iface) = split(/-/,$self->name());
    my($rname);
    $iface =~ s/:/-/g;
    # create a short and unique router name
    if ( $DHUBRouterType eq "Motorola" )     {
	# find the tas number.  If it's not a TAS, but in the entire name;.
	if ( $host =~ m/tas(\d+)/ ) {
	    $host = $1;
	}
	return "mcr$host-$iface";
    }
    if ( $DHUBRouterType eq "Toshiba" )      { $rname = "scs"; }
    else                                     { $rname = "r" ; }
    return "$tas-$rname$iface";
}
#
# sub tas_ip() -- return a tas ip (assuming a tas->router network)
sub tas_ip {
    my($self) = @_;
    my(@jnk) = $self->valid_hosts();
    return (shift(@jnk));
}
    
#
# sub router_ip() -- return a router ip (assuming a tas->router network)
sub router_ip {
    my($self) = @_;
    my(@jnk) = $self->valid_hosts();
    return (pop(@jnk));
}
#
# block2nets("a.b.c.d/X:Y") -- return an array of networks "i.j.k.l/Y" taken
# from the a.b.c.d/X block
sub block2nets {
    my($self,$arg) = @_;
    my($net,$block,$netmask) = split(/[\/:]/,$arg);
    my($classbits,$classnet,@array,$newnet);
    #
    # If someone is trying to be tricky by specifying a block that has
    # fewer bits than its class, then split that block up into many
    # Class-sized nets and run _block2nets on those.
    $classbits = (((ord($self->ipclass($net)) - ord('A')) + 1 ) * 8);
    return undef if ( $classbits < 0 );
    if ( $block < $classbits ) {
	foreach $classnet ( $self->_block2nets("$net/$block:$classbits") ) {
	    push(@array,$self->_block2nets("$classnet:$netmask"));
	}
	return @array;
    }
    return $self->_block2nets($arg);
}

#
# This internal routine does the real block2nets work
sub _block2nets {
    my($self,$arg) = @_;
    my($net,$block,$netmask) = split(/[\/:]/,$arg);
    my($netnum,$nw,$newbits,$shiftwidth);
    local($_,@array);

    return ($arg) if ( $netmask eq "" );
    #
    # Figure out the number of extra bits we're going to be using
    $newbits = int($netmask) - int($block);
    return ($arg) if ( $newbits <= 0 );
    #
    # The shift width for network numbers is 32 - $netmask
    $shiftwidth = 32 - $netmask;
    #
    # Create a new network object.
    $nw = $self->new();
    $nw->set("$net/$block");
    #
    # Figure out the network number using the block mask
    $netnum = ip2l($nw->ip()) & ip2l($nw->netmask());
    #
    # Create the array
    foreach ( 0..((1<<$newbits)-1)) {
	push(@array,l2ip(pack("L",$netnum + ($_<<$shiftwidth))) . "/$netmask");
    }
    # If the number of bits in the netmask is not on a byte boundry,
    # we need to discard the first and last nets.
    if ( $netmask % 8 != 0 ) {
	shift(@array);
	pop(@array);
    }
    return @array;
}

#
# sortip(@lst) -- assuming a list of IP or Network numbers, sort them
# and return the sorted list
sub sortip {
    my($self) = shift(@_);
    return sort _sort_ip_routine @_;
}
sub _sort_ip_routine {
    my(@abytes) = split(/[\.\/]/,$a);
    my(@bbytes) = split(/[\.\/]/,$b);
    my($val,$i);
    for ( $i = 0 ; $i <= $#abytes ; $i++) {
        $val =  $abytes[$i] <=> $bbytes[$i];
        return $val if ( $val );
    }
    # At this point they're equal.  If b is longer, it's greater
    return ( $#bbytes <= $#abytes );
}
    
#
# networkpart($ip,$mask) -- return the network number of ip using mask.
sub networkpart {
    my($self,$ip,$mask) = @_;
    $mask = $self->{BITS} if ( ! $mask );
    return (l2ip(pack("L", (ip2l($ip) & (~0 << (32-$mask))))));
}
#
# hostpart($ip,$mask) -- return the network number of ip using mask.
sub hostpart  {
    my($self,$ip,$mask) = @_;
    $mask = $self->{BITS} if ( ! $mask );
    return (l2ip(pack("L", (ip2l($ip) & ~(~0 << (32-$mask))))));
}
1;
