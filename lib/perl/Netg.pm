#
# $Id: Netg.pm 28 2005-07-26 18:12:41Z erickson $
# $Source: /home/erickson/public_html/java.ici/public_html/NetDist/NetDist/lib/perl/RCS/Netg.pm,v $
#
package Netg;

=head1 NAME

Netg - access to a 'netgroup' type database

=head1 SYNOPSIS

	use Netg;
	$ng = new Netg "somefile";
	foreach $element ( $ng->expand('mygroup') ) {
		&something($element);
	}
	@hosts =  $ng->expand_host($someng);		# just the host part
	@users =  $ng->expand_user($someng);		# just the user part
	@domains =  $ng->expand_domain($someng);	# just the domain part


=head1 DESCRIPTION

The C<Netg> method provides access to a simple 'netgroup' style database.  If no filename
is specified when the object is created, then the command 'ypcat -k netgroup' is used
to gather the NetGroup information.

=cut

#
require 5.000;
use Carp;

local(%NETG);

sub new {
	if ( $#_ < 0 || $#_ > 1 ) {
		croak 'usage new Netg [FILENAME]';
	}
	my $class = shift;
	my $self = {};
	#
	# Try to read the file
    Netg::readfile($_[0]) || return undef;
	bless $self,$class;
}

#
# readfile($file)
sub readfile {
	@_ == 1 || croak 'usage:  $ng->readfile(filename)';
	my($fn) = shift(@_);
	my($oldrs, $line, $key, $data);
	local($_);
	$fn = "ypcat -k netgroup|" if ( "$fn" eq "" ) ;
	if ( ! open(INFILE,$fn) ) {
		warn "cannot open $fn:  $!\n";
		return 0;
	}
	$oldrs = $/;	# read in the entire file
	undef $/;
	$_ = <INFILE>;
	close INFILE;
	$/ = $oldrs;
	#
	# Remove comments
	s/\#[^\n]*\n//g;
	#
	# Remove continuation lines
	s/\\\n/ /g;
	#
	# Replace all tabs & spaces whitespace with a single space
	s/[ \t]+/ /g;
	#
	# Zero out the current NETG
	undef %NETG;
	#
	# Foreach line, add the data
	foreach $line ( split(/\n/,$_) ) {
		($key,$data) = split(/\s/,$line,2);
		$NETG{$key} = $data;
	}
	return 1;
}
#
# getgroup(netgroup) -- return the group without expanding
sub getgroup {
    my($self,$ng) = @_;
    
    return () if ( $ng eq "" );
    return () if ( ! defined($NETG{$ng}) );
    return split(/\s+/,$NETG{$ng});
}
#
# expand(netgroup) 
#   return @list
#
# Expand the netgroup "netgroup", returning a list
#
# @Expanding is a list of groups that we're expanding.  We'll use this
# list to avoid loops.
sub expand {
	my($self,$ng) = @_;
	local(@ret);

	return () if ( $ng eq "" );
	# If we're already checking this group, just return an empty list.
	return () if ( &inlist($ng,@Expanding) );
	return () if ( ! defined($NETG{$ng}) );
	#
	# Ok.  We need to expand this group.  Add it to the @Expanding list.
	push(@Expanding, $ng);
	#
	# For each 'host' in the list, expand any names that are groups.
	foreach $_ ( split(/\s+/,$NETG{$ng}) ) {
		# If it's a tuple, add to our return list
		if ( /^\(/ ) {
			push(@ret,$_);
			next;
		}
		# It's not a tuple.  Expand it
		push(@ret,$self->expand($_));
	}
	#
	# We're finished expanding this group
	pop(@Expanding);
	#
	# Return a uniq'd list
	return &uniq(@ret);
}


#
# sub inlist($key,@list) -- return 1 if key is in @list
sub inlist {
	my($key,@list) = @_;
	local($_);
	foreach ( @list ) {
		return 1 if ( $_ eq $key );
	}
	return 0;
}
#
# sub uniq($key,@list) -- return a unique list
sub uniq {
	my($prev);
	local($_,@newlist);
	foreach ( @_ ) {
	    next if ( inlist($_,@newlist) );
	    push(@newlist,$_);
	}
	return @newlist;
}

#
# sub expand_field($field,$ng) -- expand a netgroup, returning only the field
# requested.
sub expand_field {
    my($self,$field,$ng) = @_;
    local($i,@grp);
    foreach $i ( $self->expand($ng) ) {
	$i =~ s/[()]//g;
	push(@grp,(split(/,/,$i))[$field]);
    }
    return @grp;
}
sub expand_host {my($self) = shift(@_); $self->expand_field(0,@_);}
sub expand_user {my($self) = shift(@_); $self->expand_field(1,@_);}
sub expand_domain {my($self) = shift(@_); $self->expand_field(2,@_);}
1;
