#
# $Id: ExpnLine.pm 28 2005-07-26 18:12:41Z erickson $
# $Source: /RR-0.14/NetDist.2/site/lib/perl/RCS/ExpnLine.pm,v $
#
package ExpnLine;

=head1 NAME

ExpnLine - expand variables in a line to create many lines

=head1 SYNOPSIS

	use ExpnLine;
	$el = new ExpnLine;
	$el->set(@lines);		# set the "src" lines
	$el->append(@lines);		# append to "src" lines
	$el->expand();			# return list of expanded lines
	$el->errors();			# return list of lines with errors


=head1 DESCRIPTION

The C<ExpnLine> package provides a method for taking a line with a special
expression and expanding that into many lines.

The C<ExpnLine> package uses the C<Site::Network> package to expand network
expressions, and uses the C<Site::Hosts> package to expand IP expressions.

The expression is bound by curly braces.  The expression is parsed in the 
following order.  The left-most-inner-most expression is parsed first.

The expression can be a comma-separated list of other expressions.

    -  word:	a simple word is unparsed

    Example: 	pass in quick from {tas1,tas2,tas3}

    Output:  	pass in quick from tas1
		pass in quick from tas2
		pass in quick from tas3

    -  $scalar: the scalar is expanded.  The variable should be set by the
    	calling routine:

    Example:	pass in quick from {$this_tas}

    Output: (assume $this_tas == "tas3")
		pass in quick from tas3

    -  @array: the array is expanded.  The variable should be set by the
    	calling routine:

    Example:	pass in quick on {@ifaces}

    Output: (assume @ifaces == ("le0", "nf0", "nf1"))
		pass in quick on le0
		pass in quick on nf0
		pass in quick on nf1


    -  _netgroup: the netgroup is expanded.

    Example:	pass in quick from {_proxye}

    Output: 
		pass in quick from proxye1
		pass in quick from proxye2
		pass in quick from proxye3
		pass in quick from proxye4


    -  ip:expr: the IP address of expr

    Example:	pass in quick from {ip:tas1}

    Output: 
		pass in quick from 10.1.3.5


    -  nw:expr: the network address of expr

    Example:	pass in quick from {nw:he-10bt}
		pass in quick from {nw:24.92.64.0/19}

    Output: 
		pass in quick from 10.1.3.0
		pass in quick from 24.92.64.0

    -  nm:network: the netmask of network

    Example:	pass in quick from {nm:he-100bt}
		pass in quick from {nm:24.92.64.0/19}

    Output: 
		pass in quick from 255.255.255.0
		pass in quick from 255.255.224.0

Nesting can be done:

    Example:	block from {ip:{tas1,tas2,tas3}}
    Output:	block from 10.1.3.1
		block from 10.1.3.2
		block from 10.1.3.3

Internally, the example looks something like:

    pass1:	block from {ip:tas1}
		block from {ip:tas2}
		block from {ip:tas3}
    pass2:	block from 10.1.3.1
		block from 10.1.3.2
		block from 10.1.3.3

Be careful to nest properly.  Without the internal set of braces, the above
example would produce the following results:

    Input:	block from {ip:tas1,tas2,tas3}

    pass1:	block from {ip:tas1}
		block from {tas2}
		block from {tas3}

    pass2:	block from 10.1.3.1
		block from tas2
		block from tas3

=head1 FUNCTIONS

=cut

#
require 5.000;
use Carp;
use FileHandle;
use Netg;
use Site::Networks;
use Site::Hosts;

local($ng);
local($nw);
local($sh);
sub new {
    my $class = shift;
    my $self = {};
    $ng = new Netg "$ENV{'NDHOME'}/etc/netgroup" if ( ! $ng );
    $nw = new Site::Networks if ( ! $nw );
    $sh = new Site::Hosts if ( ! $sh );
    #
    $self->{_SRCLINES} = [ @_ ];
    $self ->{_ERRORS} = [];
    bless $self,$class;
}

=pod

=over 4

=item errors()

Return a list of lines containing error messages.

=back

=cut

sub errors {
    my($self) = shift;
    return @{$self->{_ERRORS}};
}

=pod

=over 4

=item set(@lines)

Set the "source" lines.  These are the lines that will eventually be expanded.

=back

=cut

sub set {
    my($self) = shift;

    $self->{_SRCLINES} = [ @_ ];
}

=pod

=over 4

=item append(@lines)

Append @lines to the "source" lines.  These are the lines that will
eventually be expanded.

=back

=cut

sub append {
    my($self) = shift;
    push(@{$self->{_SRCLINES}}, @_);
}

=pod

=over 4

=item expand()

Expand the "source" lines and return the resulting lines.

=back

=cut

sub expand {
    my($self) = shift;
    my (@errors) = ();
    my (@ret) = _parse_lines(@{$self->{_SRCLINES}});
    $self->{_ERRORS} = [ @errors ];
    return @ret;
}

# _parse_lines(@lines)
# parse the lines passed, and return
# a list of lines with all "variables" expanded.
#
# A  variable consists of the data between two non-nesting '{}' characters.
# The data is a comma-separated list of tokens.
# If the token starts with a @, it's an array
# If the token starts with a $, it's a scalar
# If the token starts with a _, it's a netgroup
# If the token starts with ip:, it's the ip of the host following
# If the token starts with nw:, it's the network
# If the token starts with nm:, it's the netmask
# If the token starts with [a-zA-Z], just pass it through
#
# This is recursive.  It matches the left-most token first, then re-parses
# the line.  We quit the recursion by finding no {} pairs.
sub _parse_lines {
    local($_);
    my(@ret,$var,$begin,$match,$end);
    #
    foreach ( @_ ) {
	# match on the inner-most braces
	if ( m/(.*)\{([^\}]*)\}(.*)/s ) {
	    ($begin,$match,$end) = ($1,$2,$3);
	}
	else {
	    push(@ret,$_); # end recursion
	    next;
	}
	# Break up the stuff between the braces.  We shouldn't see any
	# more braces, because of the above match.
	foreach ( split(/,/,$match) ) {
	    # see what kind of var it is
	    # ip addr
	    if ( m/^ip:(.*)/ ) {
		$var = $sh->name2ip($1);
		if ( $var eq "" ) {
		    # oops
		    push(@errors,"$_: no IP");
		}
		else {
		    push(@ret,_parse_lines("${begin}${var}${end}"));
		}
		next;
	    }
	    # network addr
	    if ( m/^nw:(.*)/ ) {
		$nw->set($1);
		$var = $nw->ip();
		if ( $var eq "" ) {
		    # oops
		    push(@errors,"$_:  no network");
		}
		else {
		    push(@ret,_parse_lines("${begin}${var}${end}"));
		}
		next;
	    }
	    # network mask
	    if ( m/^nm:(.*)/ ) {
		$nw->set($1);
		$var = $nw->netmask();
		if ( $var eq "" ) {
		    # oops
		    push(@errors,"$_: no netmask");
		}
		else {
		    push(@ret,_parse_lines("${begin}${var}${end}"));
		}
		next;
	    }
	    # netgroup
	    if ( m/^_(.*)/ ) {
		foreach $var ( $ng->expand_host($1) ) {
		    push(@ret,_parse_lines("${begin}${var}${end}"));
		}
		next;
	    }
	    # array or scalar
	    if ( m/^([\@\$])(.*)/ ) {
		foreach $var ( eval "${1}main::$2" ) {
		    push(@ret,_parse_lines("${begin}${var}${end}"));
		}
		next;
	    }
	    # otherwise, just pass it through without the braces
	    push(@ret,_parse_lines("${begin}${_}${end}"));
	}
    }
    return @ret;
}
1;
