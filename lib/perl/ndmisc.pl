;#
;# $Id: ndmisc.pl 28 2005-07-26 18:12:41Z erickson $
;# $Source: /home/erickson/public_html/java.ici/public_html/NetDist/NetDist/lib/perl/RCS/ndmisc.pl,v $
;#
=pod

=head1 NAME

ndmisc.pl - misc. utility routines

=head1 SYNOPSIS

require ndmisc.pl;

    if ( get_yn($prompt,$timeout,$timeoutval) ) { ... }
    if ( in_list($key,@list) ) { ... }
    $idx = lindex($start,$key,@list);
    $str = basename($path, $exten);
    $str = dirname($path);
    @os_info = nd_os_info($root);

=head1 DESCRIPTION

The ndmisc.pl provides some handy routines that can be used in any script.

=head1 FUNCTIONS

=over 4

=item $bool = get_yn($prompt,$timeout,$timeoutval)

Query the user with the given $prompt and wait for a yes/no
answer.

An answer that starts with a 'y' or 'Y' means "yes".  Anything
else means "no"

=back

    return 1 for "yes"
    return 0 for "no"
    return $timeoutval for no answer within $timeout seconds

=cut 

sub get_yn {
    local($prompt,$timeout,$timeoutval) = @_;
    local($ans,$oldflush,$oldfh);
    local($rin, $rout, $nfound, $timeleft);

    $oldfh = select(STDERR);
    $oldflush = $|;
    $| = 1;
    print STDERR "$prompt";			# print out the prompt
    if ( $timeout > 0 ) {
	#
	# set up for the select() call
	$rin = '';
	vec($rin,fileno(STDIN),1) = 1;
	($nfound,$timeleft) = select($rout=$rin, undef, undef, $timeout);
	#
	# If no input, print a return, then return the timeout val
	if ( $nfound == 0 ) {
	    print STDERR "\n";
	    select($oldfh);
	    $| = $oldsel;
	    return $timeoutval;
	}
    }
    $ans = <STDIN>;
    $| = $oldsel;
    select($oldfh);
    return 1 if ( $ans =~ /^[Yy]/ );
    return 0;
}
;########################################################################
;# End of get_yn
;########################################################################

=pod

=over 4

=item $bool = in_list($key,@list)

Look for the string $key in each item of @list.

=back

     return 1 if $key is in the list
     return 0 if $key or @list is empty
     return 0 otherwise

=cut 

sub in_list {
    local($key,@list) = @_;

    return 0 if ( "$key" eq "" || $#list < 0 );
    return 1 if ( &lindex(0,$key,@list) >= 0 );
    return 0;
}
;########################################################################
;# End of in_list
;########################################################################

;#####################################################################
=pod

=over 4

=item $i = lindex($start,$key,@list)

return the index number of $key in @list starting at index $start.

=back

    return -1 if not in list

=cut

sub lindex {
    local($start) = shift;
    local($key) = shift;
    local(@list) = @_;
    local( $idx, $max );
    $max = $#list;

    for ( $idx = $start ; $idx <= $max ; $idx++ ) {
	return $idx if ( $list[$idx] =~ /^$key/ );
    }
    return -1;
}
;########################################################################
;# end of lindex
;########################################################################

=pod

=over 4

=item $str = basename($path [, $exten])

Like the UNIX command, return the last element of $path, optionally removing
an extention.  See I<basename(1)> man page for more information.

=back

=cut 

sub basename {
    local($file) = shift;
    local($exten) = shift;
    local(@lst,$nfile);

    return $file if ( "$file" eq "" );
    @lst = split(/\//,$file);
    $nfile = pop(@lst);
    $nfile =~ s/$exten$//;
    return $nfile;
}
;########################################################################
;# end of basename
;########################################################################

=pod

=over 4

=item $str = dirname($path)

Like the UNIX command, return the directory name of $path.
See I<dirname(1)> man page for more information.

=back

=cut 

sub dirname {
    local($file) = shift;
    local(@lst,$fname);

    @lst = split(/\//,$file);
    pop(@lst);
    return "" if ( $#lst < 0 );
    $fname = join("/", @lst);
    return $fname;
}
;########################################################################
;# end of dirname
;########################################################################
1;
