#
# $Id: env.sh 2062 2009-05-26 23:30:33Z dchang $
# $Source$
#
# Set up your environment to run this version of NetDist.
#
# This should be a symlink in each customer's directory.
#
NDHOME=`pwd`
export NDHOME
#
# Set up the path
PATH=${NDHOME}/bin:$PATH
PATH=`${NDHOME}/bin/fixpath PATH`
export PATH

[ -f ./env.$USER ] && . ./env.$USER
